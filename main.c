#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>
#include <endian.h>
#ifdef __ADDRESS_ENCODED_BYTE_ORDER
#include "aebo.h"
#endif
#include "dc.h"

int main(int argc,char *argv[])
{
    FILE *input_file, *output_file;
    struct stat input_file_buf;
    int32_t *table,offset;
    size_t size,length;
#ifndef __ADDRESS_ENCODED_BYTE_ORDER
    size_t index;
#endif
    enum endian {big, little, undefined};
    enum endian selected_endian;

    if(argc!=4){
        fprintf(stderr,"Invalid number of parameters\n"); exit(1);
    }
    selected_endian= argv[3][0]=='b'? big : argv[3][0]=='l'? little : undefined;
    if(selected_endian==undefined || argv[3][1]!='\0'){
        fprintf(stderr,"Wrong byte order specifier '%s'\n",argv[3]);
        exit(2);
    }
    // allocate memory and read data file
    if((input_file=fopen(argv[1],"rb")) == NULL) {
        fprintf(stderr,"Can't open source file '%s'\n",argv[1]);
        exit(3);
    }
    fstat(fileno(input_file), &input_file_buf);
    size = input_file_buf.st_size;
    length=size>>2;
    if(size&3) {
        fprintf(stderr,"The source file '%s' size is not multiple of 4\n",argv[1]);
        exit(4);
    }
    if(!size) {
        fprintf(stderr,"The source file '%s' is empty\n",argv[1]);
        exit(5);
    }
    if((table=malloc(size))==NULL){
        fprintf(stderr,"Memory exhausted");
        exit(6);
    }
    if(!fread(table,size,1,input_file)){
        fprintf(stderr,"Can't read input file '%s' \n",argv[1]);
        exit(7);
    }
    fclose(input_file);

#ifndef __ADDRESS_ENCODED_BYTE_ORDER
# if __BYTE_ORDER == __BIG_ENDIAN
    if(selected_endian==little)//reorder the bytes of the elements from little-endian to host
        for(index=0;index<length;index++)
            table[index]=le32toh(table[index]);
# else//host byte order is __LITTLE_ENDIAN
    if(selected_endian==big)//reorder the bytes of the elements from big-endian to host
        for(index=0;index<length;index++)
            table[index]=be32toh(table[index]);
# endif
#endif

#ifdef __ADDRESS_ENCODED_BYTE_ORDER
    offset=dc_offset(selected_endian==big? bea(table) : lea(table),length);//data will be read in the desired byte order
    add_dc(-offset,selected_endian==big? bea(table) : lea(table),length);//data will be read and written in the desired byte order
#else
    offset=dc_offset(table,length);
    add_dc(-offset,table,length);
#endif

#ifndef __ADDRESS_ENCODED_BYTE_ORDER
# if __BYTE_ORDER == __BIG_ENDIAN
    if(selected_endian==little)//reorder the bytes of the elements from host to little-endian
        for(index=0;index<length;index++)
            table[index]=htole32(table[index]);
# else//host byte order is __LITTLE_ENDIAN
    if(selected_endian==big)//reorder the bytes of the elements from host to big-endian
        for(index=0;index<length;index++)
            table[index]=htobe32(table[index]);
# endif
#endif
    //save the data
    if((output_file=fopen(argv[2],"wb"))==NULL){
        fprintf(stderr,"Can't open target file '%s' \n",argv[2]);
        exit(8);
    }
    if(!fwrite(table, size, 1, output_file)){
        fprintf(stderr,"Can't write target file '%s' \n",argv[2]);
        exit(9);
    }
    exit(0);
}
