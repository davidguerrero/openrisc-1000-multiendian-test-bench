#define dc_h__
#include <stdint.h>
#include <stdlib.h>

int32_t dc_offset(int32_t table[],size_t length);
void add_dc(int32_t dc,int32_t table[],size_t length);
