#ifndef dc_h__
#include "dc.h"
#endif

int32_t dc_offset(int32_t table[],size_t length)
{
  size_t index;
  int32_t sum=0;
  for(index=0;index<length;index++)
    sum+=table[index];
  return sum/length;
}

void add_dc(int32_t dc,int32_t table[],size_t length)
{
  size_t index;
  for(index=0;index<length;index++)
    table[index]+=dc;
}
