#ifndef _AEBO_H
#define _AEBO_H

#ifndef _ENDIAN_H
#include <endian.h>
#endif

#if (__BYTE_ORDER != __LITTLE_ENDIAN && __BYTE_ORDER !=__BIG_ENDIAN)
#error "This version of the library does not support the host byte order yet."
#endif

#define __reverse_endian_address(x) ((typeof(x)) ((void *)(x) + sizeof(*(x)) - 1))
/*                                   ----------- ------------   -----------------
                                         (3)         (1)               (2)
   1. Convert to void* to have unit pointer arithmetic.
   2. Set less significant address bits to 1: use non-native (reverse) byte order.
   3. Restore the original pointer type.
*/
#define __reverse_endian(x) (*__reverse_endian_address(&(x)))

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define le(x) (x)
#define lea(x) (x)
#define be(x) __reverse_endian(x)
#define bea(x) __reverse_endian_address(x)
#define pdpa(x) ((typeof(x)) ((void *)(x) + (sizeof(*(x))>2?2:0) ))
#define pdp(x) (*pdpa(&(x)))
#endif
#if __BYTE_ORDER == __BIG_ENDIAN
#define le(x) __reverse_endian(x)
#define lea(x) __reverse_endian_address(x)
#define be(x) (x)
#define bea(x) (x)
#define pdpa(x) ((typeof(x)) ((void *)(x) + (sizeof(*(x))>1?1:0) ))
#define pdp(x) (*pdpa(&(x)))
#endif

#endif
