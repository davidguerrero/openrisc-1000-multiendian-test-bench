input_size=250 #in Kbytes
number_of_iterations=100

echo "Original architecture." > original.txt
echo "address encoded byte order architecture." > AEBO.txt
for i in $(seq 1 $number_of_iterations)
do
	dd bs=1024 count=$input_size </dev/urandom >input.bin
	echo iteration $i.
	time ./no_AEBO_dc_filter input.bin output.bin l 2>> original.txt
	time ./AEBO_dc_filter input.bin output.bin l 2>> AEBO.txt
done
