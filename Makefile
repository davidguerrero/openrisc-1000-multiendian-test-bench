executables = no_AEBO_dc_filter AEBO_dc_filter
all: $(executables)
CC             = or1k-linux-musl-gcc
CFLAGS=-I.

dc.o: dc.c
	$(CC) -c $< -o $@ $(CFLAGS)

no_AEBO_dc_filter.o: main.c
	$(CC) -c $< -o $@ $(CFLAGS)

AEBO_dc_filter.o: main.c
	$(CC) -D__ADDRESS_ENCODED_BYTE_ORDER="" -c $< -o $@ $(CFLAGS)

%_dc_filter: %_dc_filter.o dc.o
	$(CC) $^ -o $@ $(CFLAGS)

.PHONY: clean

clean:
	rm -rf *.o $(executables)
